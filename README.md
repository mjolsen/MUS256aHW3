# Yet Another Polyphonic Synthesizer - MUS256a, HW #3

This application is a audio plugin that implements a polyphonic midi synthesizer. The app consists of a single bandlimited pulsetrain oscillator which features controls over the duty cycle (amount of time pulse is non-zero during a single period of the signal) and detune (from -19 to 19 cents). The plugin also features an ADSR and a resonant lowpass filter whose cutoff frequency and filter Q can be controlled. The detune amount, duty cycle, cutoff frequency and Q can be controlled by midi controller numbers 41 - 44, respectively.

---

Implemented by Michael Olsen (mjolsenATccrmaDOTstanfordDOTedu) based on starter code by Romain Michon (rmichonATccrmaDOTstanfordDOTedu) (and the Juce example "audio plugin demo") for Music 256a / CS 476a (fall 2016).
