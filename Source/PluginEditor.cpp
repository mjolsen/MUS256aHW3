/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
// This is a handy slider subclass that controls an AudioProcessorParameter
// (may move this class into the library itself at some point in the future..)
//
// ***Taken from /JUCE/examples/audio plugin demo
class Mus256aHw3AudioProcessorEditor::ParameterSlider  : public Slider,
                                                         private Timer
{
public:
    ParameterSlider (AudioProcessorParameter& p, Mus256aHw3AudioProcessorEditor& e)
        : Slider (p.getName (256)), param (p), editor(e)
    {
        setRange (0.0, 1.0, 0.0);
        startTimerHz (30);
        updateSliderPos();
    }

    void valueChanged() override
    {
        if (isMouseButtonDown())
            param.setValueNotifyingHost ((float) Slider::getValue());
        else
            param.setValue ((float) Slider::getValue());
        
        editor.passParameterToProcessor (param.getName(256));
    }

    void timerCallback() override       { updateSliderPos(); }

    void startedDragging() override     { param.beginChangeGesture(); }
    void stoppedDragging() override     { param.endChangeGesture();   }

    double getValueFromText (const String& text) override   { return param.getValueForText (text); }
    String getTextFromValue (double value) override         { return param.getText ((float) value, 1024); }

    // get textbox width/height
    int getTextBoxWidth() const noexcept                    { return Slider::getTextBoxWidth(); }
    int getTextBoxHeight() const noexcept                    { return Slider::getTextBoxHeight(); }
    
    void updateSliderPos()
    {
        const float newValue = param.getValue();

        if (newValue != (float) Slider::getValue() && ! isMouseButtonDown())
            Slider::setValue (newValue);
    }

    AudioProcessorParameter& param;
    Mus256aHw3AudioProcessorEditor& editor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ParameterSlider)
};

//==============================================================================
Mus256aHw3AudioProcessorEditor::Mus256aHw3AudioProcessorEditor (Mus256aHw3AudioProcessor& p)
    : AudioProcessorEditor (&p), //processor (p),
      midiKeyboard (p.keyboardState, MidiKeyboardComponent::horizontalKeyboard),
      attackLabel (String(), "Attack"), decayLabel (String(), "Decay"),
      sustainLabel (String(), "Sustain"), releaseLabel (String(), "Release"),
      oscDetuneLabel (String(), "Osc. Detune"), oscDutyLabel (String(), "Sq. Wave Duty"), 
      vcfFreqLabel (String(), "Cutoff Freq"), vcfQLabel (String(), "Filter Q"),
      titleLabel (String(), "Yet Another Polyphonic Synth")
{
    titleFont = Font ("URW Chancery L", 40.0f, Font::bold|Font::italic);
    labelFont = Font ("Cantarell", 17.0f, Font::bold);
    
    // add our sliders
    addAndMakeVisible (attackSlider = new ParameterSlider (*p.attackParam, *this));
    attackSlider->setSliderStyle (Slider::LinearVertical);
    attackSlider->setTextBoxStyle (Slider::TextBoxBelow, true, attackSlider->getTextBoxWidth()*0.75, attackSlider->getTextBoxHeight());
    attackSlider->setTextValueSuffix (" s");
    attackSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    attackSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    attackSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    attackSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));
    
    addAndMakeVisible (decaySlider = new ParameterSlider (*p.decayParam, *this));
    decaySlider->setSliderStyle (Slider::LinearVertical);
    decaySlider->setTextBoxStyle (Slider::TextBoxBelow, true, decaySlider->getTextBoxWidth()*0.75, decaySlider->getTextBoxHeight());
    decaySlider->setTextValueSuffix (" s");
    decaySlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    decaySlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));
    
    addAndMakeVisible (sustainSlider = new ParameterSlider (*p.sustainParam, *this));
    sustainSlider->setSliderStyle (Slider::LinearVertical);
    sustainSlider->setTextBoxStyle (Slider::TextBoxBelow, true, sustainSlider->getTextBoxWidth()*0.75, sustainSlider->getTextBoxHeight());
    sustainSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    sustainSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (releaseSlider = new ParameterSlider (*p.releaseParam, *this));
    releaseSlider->setSliderStyle (Slider::LinearVertical);
    releaseSlider->setTextBoxStyle (Slider::TextBoxBelow, true, releaseSlider->getTextBoxWidth()*0.75, releaseSlider->getTextBoxHeight());
    releaseSlider->setTextValueSuffix (" s");
    releaseSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    releaseSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (oscDetuneSlider = new ParameterSlider (*p.oscDetuneParam, *this));
    oscDetuneSlider->setSliderStyle (Slider::Rotary);
    oscDetuneSlider->setTextBoxStyle (Slider::TextBoxBelow, true, oscDetuneSlider->getTextBoxWidth(), oscDetuneSlider->getTextBoxHeight());
    oscDetuneSlider->setTextValueSuffix (" c");
    oscDetuneSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    oscDetuneSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    oscDetuneSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xff2d2e33));
    
    addAndMakeVisible (oscDutySlider = new ParameterSlider (*p.oscDutyParam, *this));
    oscDutySlider->setSliderStyle (Slider::Rotary);
    oscDutySlider->setTextBoxStyle (Slider::TextBoxBelow, true, oscDutySlider->getTextBoxWidth(), oscDutySlider->getTextBoxHeight());
    oscDutySlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    oscDutySlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    oscDutySlider->setColour (Slider::rotarySliderFillColourId, Colour (0xff2d2e33));
    
    addAndMakeVisible (vcfFreqSlider = new ParameterSlider (*p.vcfFreqParam, *this));
    vcfFreqSlider->setSliderStyle (Slider::Rotary);
    vcfFreqSlider->setTextBoxStyle (Slider::TextBoxBelow, true, vcfFreqSlider->getTextBoxWidth(), vcfFreqSlider->getTextBoxHeight());
    vcfFreqSlider->setTextValueSuffix (" Hz");
    vcfFreqSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vcfFreqSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vcfFreqSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xff2d2e33));
    
    addAndMakeVisible (vcfQSlider = new ParameterSlider (*p.vcfQParam, *this));
    vcfQSlider->setSliderStyle (Slider::Rotary);
    vcfQSlider->setTextBoxStyle (Slider::TextBoxBelow, true, vcfQSlider->getTextBoxWidth(), vcfQSlider->getTextBoxHeight());
    vcfQSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vcfQSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vcfQSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xff2d2e33));
    
    // add the slider labels
    attackLabel.attachToComponent (attackSlider, false);
    attackLabel.setFont (labelFont);
    attackLabel.setJustificationType (Justification::centred);

    decayLabel.attachToComponent (decaySlider, false);
    decayLabel.setFont (labelFont);
    decayLabel.setJustificationType (Justification::centred);
    
    sustainLabel.attachToComponent (sustainSlider, false);
    sustainLabel.setFont (labelFont);
    sustainLabel.setJustificationType (Justification::centred);
    
    releaseLabel.attachToComponent (releaseSlider, false);
    releaseLabel.setFont (labelFont);
    releaseLabel.setJustificationType (Justification::centred);
    
    oscDetuneLabel.attachToComponent (oscDetuneSlider, false);
    oscDetuneLabel.setFont (labelFont);
    oscDetuneLabel.setJustificationType (Justification::centred);
    
    oscDutyLabel.attachToComponent (oscDutySlider, false);
    oscDutyLabel.setFont (labelFont);
    oscDutyLabel.setJustificationType (Justification::centred);
    
    vcfFreqLabel.attachToComponent (vcfFreqSlider, false);
    vcfFreqLabel.setFont (labelFont);
    vcfFreqLabel.setJustificationType (Justification::centred);
    
    vcfQLabel.attachToComponent (vcfQSlider, false);
    vcfQLabel.setFont (labelFont);
    vcfQLabel.setJustificationType (Justification::centred);
    
    // the title label
    addAndMakeVisible (titleLabel);
    titleLabel.setFont (titleFont);
    titleLabel.setColour (0x1000281,Colour(0xaad2d5f6));
    
    // add the midi keyboard
    addAndMakeVisible (midiKeyboard);
    
    // set the resize limits
    setResizeLimits (650, 300, 1200, 800);
    
    // set size based on last stored size
    setSize (p.lastUIWidth, p.lastUIHeight);
}

Mus256aHw3AudioProcessorEditor::~Mus256aHw3AudioProcessorEditor()
{
}


//==============================================================================
void Mus256aHw3AudioProcessorEditor::paint (Graphics& g)
{
    g.setGradientFill (ColourGradient (Colour(0xff191990), 0.125*(float) getWidth(), 0.125*(float) getHeight(),
                                       Colour(0xff4c5865), 0.875*(float) getWidth(), 0.875*(float) getHeight(), true));
    g.fillAll();
}

void Mus256aHw3AudioProcessorEditor::resized()
{
    // store current component size in rectangle
    Rectangle<int> r (getLocalBounds().reduced (8));
    
    midiKeyboard.setBounds (r.removeFromBottom (60));
    titleLabel.setBounds (r.removeFromTop(40).removeFromLeft(r.getWidth() * 0.4));
    
    Rectangle<int> oscAreaLeft (r.removeFromRight (r.getWidth() * 0.5).removeFromTop (r.getHeight() - 15));
    Rectangle<int> oscAreaRight (oscAreaLeft.removeFromRight (oscAreaLeft.getWidth()*0.5));
    oscDetuneSlider->setBounds (oscAreaLeft.removeFromTop (oscAreaLeft.getHeight()*0.5 - 15));
    oscDutySlider->setBounds (oscAreaLeft.removeFromBottom (oscAreaLeft.getHeight() - 30));
    vcfFreqSlider->setBounds (oscAreaRight.removeFromTop (oscAreaRight.getHeight()*0.5 - 15));
    vcfQSlider->setBounds (oscAreaRight.removeFromBottom (oscAreaRight.getHeight() - 30));
    
    r.removeFromTop (30);
    Rectangle<int> adsrArea (r.removeFromTop (r.getHeight() - 15));
    attackSlider->setBounds (adsrArea.removeFromLeft (adsrArea.getWidth() / 4));
    decaySlider->setBounds (adsrArea.removeFromLeft (adsrArea.getWidth() / 3));
    sustainSlider->setBounds (adsrArea.removeFromLeft (adsrArea.getWidth() / 2));
    releaseSlider->setBounds (adsrArea.removeFromLeft (adsrArea.getWidth() ));
    
    getProcessor().lastUIWidth = getWidth();
    getProcessor().lastUIHeight = getHeight();
}

void Mus256aHw3AudioProcessorEditor::passParameterToProcessor (String paraName)
{
    getProcessor().setParameterValues(paraName);
}