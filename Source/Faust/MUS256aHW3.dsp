import("stdfaust.lib");

// orig music.lib adsr
adsr(a,d,s,r,t) = env ~ (_,_) : (!,_) // the 2 'state' signals are fed back
with {
    env (p2,y) =
        (t>0) & (p2|(y>=1)),          // p2 = decay-sustain phase
        (y + p1*u - (p2&(y>s))*v*y - p3*w*y)	// y  = envelop signal
	*((p3==0)|(y>=eps)) // cut off tails to prevent denormals
    with {
		p1 = (p2==0) & (t>0) & (y<1);         // p1 = attack phase
		p3 = (t<=0) & (y>0);                  // p3 = release phase
		// #samples in attack, decay, release, must be >0
		na = ma.SR*a+(a==0.0); nd = ma.SR*d+(d==0.0); nr = ma.SR*r+(r==0.0);
		// correct zero sustain level
		z = s+(s==0.0)*ba.db2linear(-60);
		// attack, decay and (-60dB) release rates
		u = 1/na; v = 1-pow(z, 1/nd); w = 1-1/pow(z*ba.db2linear(60), 1/nr);
		// values below this threshold are considered zero in the release phase
		eps = ba.db2linear(-120);
    };
};

// adsr controls
attack  = vslider("attack",0.1,0.05,1,0.01);
decay   = vslider("decay",0.1,0.05,1,0.01);
sustain = vslider("sustain",0.7,0.05,1,0.01);
release = vslider("release",1.0,0.05,5.0,0.01);

// main controls
gate = checkbox("gate") : adsr(attack,decay,sustain,release);
s = nentry("s",0,0,1,0.001);
freq = vslider("freq",220,20,20000,0.01) : si.smooth(s);

// pulsetrain oscillator pair
N = 2;
osc1 = os.pulsetrainN(N,freq,oscDuty);
osc2 = os.pulsetrainN(N,freq*pow(2,(oscDetune/1200.0)),oscDuty);

// oscillator parameters
oscDetune = vslider("oscDetune",0,-99,99,0.01);
oscDuty   = vslider("oscDuty",0.5,0.01,0.99,0.01) : si.smoo;

// combined oscillators
osc = (0.5 * osc1,0.5 * osc2) :> _;

//
vcfFreq = vslider("vcfFreq",440,20,6000,0.01) : si.smoo;
vcfQ = vslider("vcfQ",0.01,0.01,10,0.01) : si.smoo;
vcf = fi.resonlp(vcfFreq,vcfQ,1);

process = hgroup("osc",osc * gate : vcf);
