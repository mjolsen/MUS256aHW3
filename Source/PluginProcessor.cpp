/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

#include "Faust/MUS256aHW3Faust.h"

//==============================================================================
class FaustSound : public SynthesiserSound
{
public:
    FaustSound() {}
    
    bool appliesToNote (int) override { return true; }
    bool appliesToChannel (int) override { return true; }
};

//==============================================================================
class FaustVoice : public SynthesiserVoice
{
public:
    FaustVoice(int numVoices) 
        : tailOff (true), 
          onOff (false), 
          envelope (0.0)
    {
        curNumVoices = numVoices;
        mus256aHw3.init (getSampleRate() );
        mus256aHw3.buildUserInterface (&mus256aHw3Control);
    }
    
    bool canPlaySound (SynthesiserSound* sound) override
    {
        return dynamic_cast<FaustSound*> (sound) != nullptr;
    }
    
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* /*sound*/,
                    int /*currentPitchWheelPosition*/) override
    {
   
        audioBuffer = new float*[1];
        audioBuffer[0] = new float[1];

        mus256aHw3Control.setParamValue ("/osc/s",0);
        mus256aHw3Control.setParamValue ("/osc/freq",MidiMessage::getMidiNoteInHertz (midiNoteNumber));
        mus256aHw3.compute (1, NULL, audioBuffer);
        
        delete [] audioBuffer[0];
        delete [] audioBuffer;
        audioBuffer = 0;
        
        mus256aHw3Control.setParamValue ("/osc/s",0.999);
        mus256aHw3Control.setParamValue ("/osc/gate",velocity);
        
        // set note status to on
        onOff = true;
    }
    
    void stopNote (float /*velocity*/, bool allowTailOff) override
    {
        onOff = false; // end the note
        mus256aHw3Control.setParamValue ("/osc/gate",0.0);
        tailOff = allowTailOff; // store the tailOff status
    }
    
    void pitchWheelMoved (int newValue) override 
    {
    }
    
    void controllerMoved (int controllerNumber, int newValue) override 
    {
        float newValConverted;
        
        // dirty hard-coded controller values handling
        // works with sliders 1, 2, 3 and 4 on the
        // Novation Impulse midi controller

        // if slider #1, control the oscillator detune value
        if (controllerNumber == 41){
            newValConverted = (38.0/127.0)*(float)newValue - 19.0;
            mus256aHw3Control.setParamValue ("/osc/oscDetune",newValConverted);
        }
        // if slider #2, control the oscillator duty value
        else if (controllerNumber == 42){
            newValConverted = (0.98/127.0)*(float)newValue + 0.01;
            mus256aHw3Control.setParamValue ("/osc/oscDuty",newValConverted);
        }
        // if slider #3, control the filter cutoff
        if (controllerNumber == 43){
            newValConverted = (7950.0/127.0)*(float)newValue + 50.0;
            mus256aHw3Control.setParamValue ("/osc/vcfFreq",newValConverted);
        }
        // if slider #4, control the filter Q
        else if (controllerNumber == 44){
            newValConverted = (49.9/127.0)*(float)newValue + 0.1;
            mus256aHw3Control.setParamValue ("/osc/vcfQ",newValConverted);
        }
    }
    
    // method to pass parameter values to Faust dsp class
    void setParameterValues (String paraName, double paraVal)
    {
        // osc values should be updated for active and inactive notes
        if (paraName == "oscDetune")
            mus256aHw3Control.setParamValue ("/osc/oscDetune",paraVal);
        else if (paraName == "oscDuty")
            mus256aHw3Control.setParamValue ("/osc/oscDuty",paraVal);
        else if (paraName == "vcfFreq")
            mus256aHw3Control.setParamValue ("/osc/vcfFreq",paraVal);
        else if (paraName == "vcfQ")
            mus256aHw3Control.setParamValue ("/osc/vcfQ",paraVal);

        // only update ADSR values if note isn't currently audible
        if (envelope < 0.001 || onOff == false){
            if (paraName == "attack")
                mus256aHw3Control.setParamValue ("/osc/attack",paraVal);
            else if (paraName == "decay")
                mus256aHw3Control.setParamValue ("/osc/attack",paraVal);
            else if (paraName == "sustain")
                mus256aHw3Control.setParamValue ("/osc/sustain",paraVal);
            else if (paraName == "release")
                mus256aHw3Control.setParamValue ("/osc/release",paraVal);
        }
    }
    
    void renderNextBlock (AudioBuffer<float>& outputBuffer, int startSample, int numSamples) override
    {
        processBlock (outputBuffer, startSample, numSamples);
    }
    
    void renderNextBlock (AudioBuffer<double>& outputBuffer, int startSample, int numSamples) override
    {
        processBlock (outputBuffer, startSample, numSamples);
    }

private:

    template <typename FloatType>
    void processBlock (AudioBuffer<FloatType>& outputBuffer, int startSample, int numSamples)
    {
        int origNumSamples = numSamples, faustOutputs = mus256aHw3.getNumOutputs();
        double polyGain = 1.0/(double)curNumVoices;
        
        // only compute block if note is on
        if (envelope != 0.0 || onOff){
            // allocate memory - done here because it is the only place where we
            // have access to the current size of the buffer
            audioBuffer = new float*[1];
            //for (int i = 0; i < faustOutputs; ++i)
                audioBuffer[0] = new float[numSamples];
            
            // compute current buffer with Faust code
            mus256aHw3.compute (numSamples, NULL, audioBuffer);
            FloatType tmpEnv = 0.0; // temp var to calc env from avg of samples
            int i = 0;
            while (--numSamples >= 0){
                // calculate tmpEnv from first output of Faust code
                tmpEnv += std::abs (static_cast<FloatType>(audioBuffer[0][i]));
                for (int channel = 0; channel < outputBuffer.getNumChannels(); ++channel){
                    FloatType curVal;
                    curVal = static_cast<FloatType> (polyGain * audioBuffer[0][i]);
                    
                    // add current value to each channel
                    outputBuffer.addSample (channel, startSample, curVal);
                }
                // increment indexes
                ++i;
                ++startSample;
            }
            envelope = tmpEnv / static_cast<FloatType>(origNumSamples);
            
            // if no tail or amplitude below threshold (-60 dB), clear note
            if (!onOff && (envelope < 0.01 || !tailOff)){
                envelope = 0.0;
                clearCurrentNote();
            }
            
            // clean up memory
            delete [] audioBuffer[0];

            delete [] audioBuffer;
            audioBuffer = 0;
        }
    }
    
    MUS256aHW3 mus256aHw3;
    MapUI mus256aHw3Control;
    int curNumVoices;
    double envelope;
    float** audioBuffer;
    bool onOff, tailOff;
};

//==============================================================================
Mus256aHw3AudioProcessor::Mus256aHw3AudioProcessor() : lastUIWidth (650), lastUIHeight (300),
                                                       attackParam (nullptr), decayParam (nullptr),
                                                       sustainParam (nullptr), releaseParam (nullptr),
                                                       oscDetuneParam (nullptr)
{
    // create parameters
    addParameter (attackParam = new AudioParameterFloat ("attack", "attack", 0.05f, 1.0f, 0.1f));
    addParameter (decayParam = new AudioParameterFloat ("decay", "decay", 0.05f, 1.0f, 0.1f));
    addParameter (sustainParam = new AudioParameterFloat ("sustain", "sustain", 0.05f, 1.0f, 0.7f));
    addParameter (releaseParam = new AudioParameterFloat ("release", "release", 0.05f, 5.0f, 1.0f));
    addParameter (oscDetuneParam = new AudioParameterFloat ("oscDetune", "oscDetune", -19.0f, 19.0f, 0.0f));
    addParameter (oscDutyParam = new AudioParameterFloat ("oscDuty", "oscDuty", 0.01f, 0.99f, 0.5f));
    addParameter (vcfFreqParam = new AudioParameterFloat ("vcfFreq", "vcfFreq", 50.0f, 8000.0f, 4000.0f));
    addParameter (vcfQParam = new AudioParameterFloat ("vcfQ", "vcfQ", 0.1f, 50.0f, 1.0f));
    initialiseSynth();
}

Mus256aHw3AudioProcessor::~Mus256aHw3AudioProcessor()
{
}

void Mus256aHw3AudioProcessor::initialiseSynth()
{
    const int numVoices = 8;
    
    // add the voices
    for (int i = numVoices; --i >= 0;)
        synth.addVoice (new FaustVoice(numVoices));
    
    synth.addSound (new FaustSound());
}

void Mus256aHw3AudioProcessor::setParameterValues(String paraName)
{       
    for (int i = 0; i < synth.getNumVoices(); ++i){
        FaustVoice* tmpVoice = (FaustVoice*)synth.getVoice(i);
        double correctVal = 0.0;
        // get parameter value in the correct range
        // couldn't figure out how to do in Editor
        if (paraName == "attack")
            correctVal = attackParam->get();
        else if (paraName == "decay")
            correctVal = decayParam->get();
        else if (paraName == "sustain")
            correctVal = sustainParam->get();    
        else if (paraName == "release")
            correctVal = releaseParam->get();
        else if (paraName == "oscDetune")
            correctVal = oscDetuneParam->get();
        else if (paraName == "oscDuty")
            correctVal = oscDutyParam->get();
        else if (paraName == "vcfFreq")
            correctVal = vcfFreqParam->get();
        else if (paraName == "vcfQ")
            correctVal = vcfQParam->get();
            
        tmpVoice->setParameterValues(paraName,correctVal);
    }
}

//==============================================================================
const String Mus256aHw3AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Mus256aHw3AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Mus256aHw3AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double Mus256aHw3AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Mus256aHw3AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Mus256aHw3AudioProcessor::getCurrentProgram()
{
    return 0;
}

void Mus256aHw3AudioProcessor::setCurrentProgram (int index)
{
}

const String Mus256aHw3AudioProcessor::getProgramName (int index)
{
    return String();
}

void Mus256aHw3AudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Mus256aHw3AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    synth.setCurrentPlaybackSampleRate (sampleRate);
    keyboardState.reset();
}

void Mus256aHw3AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
    keyboardState.reset();
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Mus256aHw3AudioProcessor::setPreferredBusArrangement (bool isInput, int bus, const AudioChannelSet& preferredSet)
{
    // Reject any bus arrangements that are not compatible with your plugin

    const int numChannels = preferredSet.size();

   #if JucePlugin_IsMidiEffect
    if (numChannels != 0)
        return false;
   #elif JucePlugin_IsSynth
    if (isInput || (numChannels != 1 && numChannels != 2))
        return false;
   #else
    if (numChannels != 1 && numChannels != 2)
        return false;

    if (! AudioProcessor::setPreferredBusArrangement (! isInput, bus, preferredSet))
        return false;
   #endif

    return AudioProcessor::setPreferredBusArrangement (isInput, bus, preferredSet);
}
#endif

template <typename FloatType>
void Mus256aHw3AudioProcessor::process (AudioBuffer<FloatType>& buffer, MidiBuffer& midiMessages)
{
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

    const int numSamples = buffer.getNumSamples();
    
    // pass any incoming midi messages to keyboard state object
    keyboardState.processNextMidiBuffer (midiMessages, 0, numSamples, true);
    
    // we need to look at our midi messages and update our AudioProcessorParameters
    // so that editor can display the changes and notify host application
    MidiBuffer::Iterator iterator (midiMessages);
    MidiMessage m;
    int sampleNumber;
    
    while (iterator.getNextEvent (m, sampleNumber)){
        if (m.isController()){
            // if mod wheel, change the value for the filter cutoff freq
            if (m.getControllerNumber() == 41){
                oscDetuneParam->beginChangeGesture();
                oscDetuneParam->setValueNotifyingHost (m.getControllerValue()/127.0);
                oscDetuneParam->endChangeGesture();
            }
            // if slider #1, control the oscillator duty value
            else if (m.getControllerNumber() == 42){
                oscDutyParam->beginChangeGesture();
                oscDutyParam->setValueNotifyingHost (m.getControllerValue()/127.0);
                oscDutyParam->endChangeGesture();
            }
            // if slider #2, control the filter Q
            else if (m.getControllerNumber() == 43){
                vcfFreqParam->beginChangeGesture();
                vcfFreqParam->setValueNotifyingHost (m.getControllerValue()/127.0);
                vcfFreqParam->endChangeGesture();
            }
            // if slider #2, control the filter Q
            else if (m.getControllerNumber() == 44){
                vcfQParam->beginChangeGesture();
                vcfQParam->setValueNotifyingHost (m.getControllerValue()/127.0);
                vcfQParam->endChangeGesture();
            }            
        }
    }
    
    // send data to synth and let it generate output
    synth.renderNextBlock (buffer, midiMessages, 0, numSamples);
    
    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
}

//==============================================================================
bool Mus256aHw3AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Mus256aHw3AudioProcessor::createEditor()
{
    return new Mus256aHw3AudioProcessorEditor (*this);
}

//==============================================================================
void Mus256aHw3AudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    
    // creat an outer XML element
    XmlElement xml ("MYPLUGINSETTINGS");
    
    // add some attributes to it
    xml.setAttribute ("uiWidth", lastUIWidth);
    xml.setAttribute ("uiHeight", lastUIHeight);
    
    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i){
        if (AudioProcessorParameterWithID* p = dynamic_cast<AudioProcessorParameterWithID*> (getParameters().getUnchecked(i)))
            xml.setAttribute (p->paramID, p->getValue());
    }
    
    copyXmlToBinary (xml, destData);
}

void Mus256aHw3AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.

    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState != nullptr)
    {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName ("MYPLUGINSETTINGS"))
        {
            // ok, now pull out our last window size..
            lastUIWidth  = jmax (xmlState->getIntAttribute ("uiWidth", lastUIWidth), 800);
            lastUIHeight = jmax (xmlState->getIntAttribute ("uiHeight", lastUIHeight), 400);

            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID* p = dynamic_cast<AudioProcessorParameterWithID*> (getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost ((float) xmlState->getDoubleAttribute (p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Mus256aHw3AudioProcessor();
}
