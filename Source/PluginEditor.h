/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class Mus256aHw3AudioProcessorEditor  : public AudioProcessorEditor
{
public:
    Mus256aHw3AudioProcessorEditor (Mus256aHw3AudioProcessor&);
    ~Mus256aHw3AudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    void passParameterToProcessor (String /*paraName*/);
    
private:
    class ParameterSlider;
    //AudioParameterBool* getParameterForButton (Button* /*button*/);
    
    MidiKeyboardComponent midiKeyboard;
    Label attackLabel, decayLabel, sustainLabel, releaseLabel, oscDetuneLabel, 
          oscDutyLabel, vcfFreqLabel, vcfQLabel, titleLabel;
    ScopedPointer<ParameterSlider> attackSlider, decaySlider, sustainSlider, 
                                   releaseSlider, oscDetuneSlider, 
                                   oscDutySlider,vcfFreqSlider, vcfQSlider;
    
    Font titleFont, labelFont;
    
    Mus256aHw3AudioProcessor& getProcessor() const
    {
        return static_cast<Mus256aHw3AudioProcessor&> (processor);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Mus256aHw3AudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
